>  Resources for learning code

### All about code
-   [Refactoring Guru](https://refactoring.guru/refactoring) - Refactoring is a systematic process of improving code
-   [Design Patterns](https://www.tutorialspoint.com/design_pattern) - Design patterns are solutions to general problems that software developers faced during software development
-   [Data Structure and Algorithm](https://www.geeksforgeeks.org/data-structures/) - A data structure is a particular way of organizing data in a computer so that it can be used effectively.
-   [Software Arcitecture](https://sourcemaking.com/)